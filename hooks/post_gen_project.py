import sys
import shutil
import os
import wget
import subprocess

def main():

    git_path= "https://gitlab.esss.lu.se/iocs/rf/rflps/e3-ioc-spk-{{ cookiecutter.ioc_name }}-sim-ioc{{ cookiecutter.plc }}.git"

    url = "https://gitlab.esss.lu.se/rflps/sim/e3ioc-spk-010-sim-ioc01/-/raw/master/st-spk-{{ cookiecutter.ioc_name }}-plc{{ cookiecutter.plc }}.cmd"
    filename = wget.download(url)
    # opening the file in read mode
    subprocess.call(["sed -i '/^epicsEnvSet(IOCNAME,/d' "+filename,], shell=True)
    subprocess.call(["sed -i '/^epicsEnvSet(IOCDIR,/d' "+filename,], shell=True)
    subprocess.call(["sed -i '/^#require rflps_sim/d' "+filename,], shell=True)
    a='"ENGINEER"/d'
    subprocess.call(["sed -i '/^epicsEnvSet("+a+"' "+filename,], shell=True)
    subprocess.call(["sed -i '/require rflps_sim/c\\require rflps_sim,0.2.0+0' "+filename,], shell=True)
    subprocess.call(["git clone "+git_path,], shell=True)
    subprocess.call(["ls",], shell=True)
    subprocess.call(["pwd",], shell=True)
    os.rename(filename, 'st.cmd')
    t='epicsEnvSet("PREFIX'
    cmd = "sed -i 's/^"+t+".*/"+'epicsEnvSet("PREFIX", "$(RFLPS_PREFIX=Spk-{{ cookiecutter.ioc_name }}RFC:)")/g'+"' st.cmd"
    print(cmd)
    subprocess.call([cmd,], shell=True)
    #subprocess.call(["mv "+filename+" st.cmd",], shell=True)
    subprocess.call(["mv ioc.yml e3-ioc-spk-{{ cookiecutter.ioc_name }}-sim-ioc{{ cookiecutter.plc }}/.",], shell=True)
    subprocess.call(["cp st.cmd e3-ioc-spk-{{ cookiecutter.ioc_name }}-sim-ioc{{ cookiecutter.plc }}/st.cmd",], shell=True)
    subprocess.call(["mv README.md e3-ioc-spk-{{ cookiecutter.ioc_name }}-sim-ioc{{ cookiecutter.plc }}/.",], shell=True)
    subprocess.call(["cp -r db/* e3-ioc-spk-{{ cookiecutter.ioc_name }}-sim-ioc{{ cookiecutter.plc }}/subs/",], shell=True)
    os.chdir('e3-ioc-spk-{{ cookiecutter.ioc_name }}-sim-ioc{{ cookiecutter.plc }}')
    subprocess.call(["git status",], shell=True)
    subprocess.call(["git add README.md ioc.yml st.cmd subs/*",], shell=True)
    subprocess.call(["git commit -m 'ICSHWI-9441: Apply Pv name changes for spk-{{ cookiecutter.ioc_name }} - ioc{{ cookiecutter.plc }}' ",], shell=True)
    os.chdir('../')
    subprocess.call(["mv e3-ioc-spk-{{ cookiecutter.ioc_name }}-sim-ioc{{ cookiecutter.plc }} ../",], shell=True)
    if '{{ cookiecutter.ioc_type }}' == 'nfs':
        os.remove("environment.yaml")
        os.chdir('../')
        subprocess.call(["rm -rf temp-{{cookiecutter.ioc_name}}-ioc{{cookiecutter.plc}}",], shell=True)
if __name__ == '__main__':
    main()



