"""
This is a bit of a strange pre-commit hook. It uses some jinja trickery (inspired by
https://github.com/samj1912/cookiecutter-advanced-demo/blob/master/hooks/pre_gen_project.py)
to split up the cookiecutter variable and treat it as a space-separated list.

{% if cookiecutter.space_separated_additional_packages != "-none-" %}
{{ cookiecutter.update({"packages":cookiecutter.space_separated_additional_packages.split()}) }}
{% else %}
{{ cookiecutter.update({"packages":[]}) }}
{% endif %}
"""
import subprocess

subprocess.call(["ls",], shell=True)
subprocess.call(["pwd",], shell=True)
print("EPAAA")